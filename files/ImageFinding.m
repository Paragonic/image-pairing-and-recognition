%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   2014-12-12
%
%   This is a short program written by
%   Max Jonsson, Rickard Lindstedt & Michael 
%   Sjostrom for a course in Image Processing
%   & Analysis at Linkopings University.
%   
%   Assembling images of type similar to the
%   Prokudin-Gorskii collection.
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

tic % for benchmarking

%%%%%%%%%%%%%% CLEAR ALL %%%%%%%%%%%%%%%
clear all;
close all;

% read an image
% CAUTION - using possible TIF images 
% may take up to 2 minutes!!!
%
myImage = readImageFromDirectory('01736a.tif');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% split it in three parts (B, G, R)

height = size(myImage, 1);
width = size(myImage, 2);

heightPerImage = uint16(height / 3);

R = myImage(heightPerImage*2 + 1:heightPerImage * 3, :);
G = myImage(heightPerImage + 1:heightPerImage * 2, :);
B = myImage(1:heightPerImage, :);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% find common points of interest

[optimizer,metric] = imregconfig('Multimodal');

% by trial and error, 40 iterations were enough
% to produce a good picture and minimizing the 
% computation time.
optimizer.MaximumIterations = 40;

registeredRG = imregister(R, G,'translation', optimizer, metric);
registeredBG = imregister(B, registeredRG,'translation', optimizer, metric);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% merge the pictures to one.

merge(:,:,1) = registeredRG;
merge(:,:,2) = G;
merge(:,:,3) = registeredBG;

figure;
imshow(merge);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% crop the image accordingly, cutting away 10% 
% (as this has been seen as a generally good measure for 
% ridding the image of the mismatched borders).

crop = merge(int16(heightPerImage * 0.1):end - int16(heightPerImage * 0.1),int16(width * 0.1):end-int16(width * 0.1), :);

figure;
imshow(crop);

toc % end benchmarking
