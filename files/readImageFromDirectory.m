function [image] = readImageFromDirectory(imageToRead)
    image = double(imread(imageToRead));
    image = image ./ max(max(image));